package br.com.thiagogbferreira;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@SpringBootApplication
@Slf4j
public class RabbitmqToApiApplication {
  @Value( "${service.endpoint:http://localhost:8080/customers}" )
  private String endpoint;
  private final RestTemplate restTemplate = new RestTemplate();

  public static void main(String[] args) throws InterruptedException {
    SpringApplication.run(RabbitmqToApiApplication.class, args);
  }

  @RabbitListener(queues = "${queue.name:QU.Customer}" )
  public void receiveMessage(Message message, Channel channel) throws IOException {
    MicroServicesContext context = (MicroServicesContext) message.getMessageProperties().getHeaders().get("context");
    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
      context.fillList(httpHeaders);
      String messageContent = new String(message.getBody());
      if (log.isDebugEnabled()) {
        log.debug("REQUEST {}", messageContent);
      }
      restTemplate.exchange(
          endpoint,
          HttpMethod.POST,
          new HttpEntity<>(messageContent,httpHeaders),
          String.class
      );
      channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
    }
  }
}
