FROM openjdk:8-jdk-alpine
LABEL version=1.0.0 \
      group=br.com.thiagogbferreira.rabbitmq \
      artifact=rabbitmq-to-api
VOLUME /tmp
COPY target/rabbitmq-to-api-1.0.0.jar app.jar
ENV JAVA_OPTS=""

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]

